#  Docker Hands-on

## Table of Contents

* [Prerequisite](#Prerequisite)
* [Install Docker](#Install Docker)
* [Simple Docker Commands](#Simple Commands)
* [Docker Containers](#Docker Containers)
* [Docker Image](#Docker Image)
* [Docker Registry](#Docker Registry)
* [Container Port](#Container Port)
* [Docker Volume](#Docker Volume)
* [Docker Stats](#Docker Stats)
* [Install Minikube](#Install Minikube )
* [Deploying a Web Server](#Deploying a Web Server)




## Prerequisite

We will use Docker version 19.03.8 to go through the hands-on exercices. Before that we need to have the following minimal Prerequisite:

### Operating System

We will use Ubuntu/Debian Linux operating system. Any other Linux operating system should be fine, provided Version 3.10 or higher of the Linux kernel.

If you have Windows operating system running in your system, install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) or [VMware](https://www.vmware.com/se/products/workstation-player/workstation-player-evaluation.html) and create a VM with our choice  of OS. Hyper-V must be enabled in BIOS.

### Virtual Machine 

Instead of creating a VM and installing Ubuntu OS, you can download an already pre-built Ubuntu VM [without GUI](https://drive.google.com/file/d/1YBgOkNNujIBjaLFTCd_5VvsOtCew_jLK/view?usp=sharing) / [with GUI](https://drive.google.com/file/d/1fGQI6jdiR9InzMHBjPGozAkTO54GLVYH/view?usp=sharing). You have to Unzip the downloaded file and use the Docker.ova file to import in your system. The instruction is given [here](https://docs.oracle.com/cd/E26217_01/E26796/html/qs-import-vm.html). 

### Git
Install Git.
``` 
sudo apt install git-all 
```

 Simple git [commands](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).   

### Simple Linux Commands
Knowledge of basic Linux commands. 


## Install Docker

In this exercice, we will install Docker from a repository. First we have to set up the repository and install Docker.

1. Update the apt package index 
```
 sudo apt-get update 
```

2.  Install packages to allow apt to use a repository over HTTPS.
```
 sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

3. Add Docker’s official GPG key
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Verify that you now have the key with the fingerprint 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88, by searching for the last 8 characters of the fingerprint.
```
sudo apt-key fingerprint 0EBFCD88
```

4. We will use the stable repository for Docker installation.
```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

5. Install latest version Docker engine using the following command. 
```
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

6. Create the docker group.
```
sudo groupadd docker
```

7. Add your user to the docker group.
```
sudo usermod -aG docker $USER
```

## Simple Docker Commands

Now we can verify the installation is complete.
```
docker info
```

You can find Docker version, Docker configurations, number of images and containers etc.
 

## Docker Containers

In this exercice, we will go through different operations on a Docker containers.

### Deploying a container
```
docker run hello-world:latest
```
It creates a container and starts the container immediately. We can pass many [parameteres](https://docs.docker.com/engine/reference/commandline/run/) in this command such as name of the container, volume, port exposure etc.  


### Creating container
```
docker create hello-world:latest
```
It creates a container but does not start. 

### List all the running container
```
docker ps 
```
List out all the running containers. Here you can find the container ID. 

```
docker ps  -a
```
List out all the containers including the pause one. 

### Attach to a container 
```
docker attach <container ID>
```
It connects to the container.

### Stopping a  container
```
docker stop <container ID>
```
It stops already running container

### Restart a  containers
```
docker restart <container ID>
```
It restarts a container from the pause state.

### Logs of container
```
docker logs <container ID>
```
It prints logs from the container.

### Container information
```
docker inspects <container ID>
```
It prints all the container information such as IP address.

### Resource usage of containers 
```
docker stats <container ID>
```
It prints the resource usage of the container such as CPU usage, memory, DIsk I/O etc.

### Changes in the container 
```
docker diff <container ID>
```
It shows changes in the container such as modification in the file system.

### Copy files in the container 
```
docker cp <source file> <container id>:<desitation>
```
It allows to copy files from the local file system to container file system.


### Copy files from container to local file system

```
docker cp <container id>:<source file> < desitation >
```
It allows to copy files from the container file system to the local file system.


### Removing a container
```
docker rm  <container ID>
```

## Docker Image
In this exercice, we will go through all the operations related to Docker image.

### Pulling an image
```
docker pull <image:tag>
```
It allows to download the image from a registry server. image, tag are the name and tag of the image respectively.

### History of an image
```
docker histrory <image:tag>
```
Shows all the modification done in the image.

### Removing an image
```
docker rmi <image:tag>
```
It allows to remove an image from the local file system. If a container is using the image, it shows an error.

### Removing all the images
```
docker image prune 
```
It allows to remove all the images.

## Creating a Docker image
In this exercice, we will create a Docker image from a Dockerfile. For this we will use the Dockerfile found in the image-builder directory. 

### docker build
```
docker build  -t <myimage:latest> .
```
myimage is the name of the image and latest is the version or tag. Now you can verify the image has been created.

### List the images
```
docker images
```
It allows to list out all the images.

## Docker Registry

In this exercice, we will use Docker hub to share our image. First you need to create an account in Docker hub.

## Login to the Docker hub
```
docker login
```
It gives us to login to the Dokcer hub account. It will ask to provide username and password of Docker hub account.

## Tagging an image
```
docker tag myimage:latest -t <user name>/myimage:latest
```
We need to rename the image before pushing the image to the Docker hub. 

## Sharing an image 
```
docker push <image:tag>
```
It uploads the Docker image in the Docker hub. 

## Container Port
In this exercice, we will create a container and expose the container port so that the we can access the application from the outside. We will use simple Nginx server and expose the port. 

### Nginx container without exposing the port
```
docker run --name mynginx1 -d nginx
```
Here we are not exopsing the port so the Nginx server is only accessible from inside the container. -d parameter is to run container in background and print container ID. 

### Nginx container with exposing the port
```
docker run --name mynginx1 -p 80:80 -d nginx
```
Now you can use access the Nginx server by using web browser or curl command.


## Docker Volume

In this exercise, we will use Docker volume and mount files to the container file system. 

### Creating volume
```
docker volume create <volume name>
```
It creates the volume.

### List out volumes
```
docker volume  ls
```
It list out all the volumes.

### Inspect the propeties of a volume 
```
docker volume inspect <volume name>
```
It shows the properties of the volume.

### Deploy a container using  volume
We will create an image using the Dockerfile given in the image-builder2. This Dockerfile does not copy the server files (index.html, docker.png). We will mount the two files into the container file system using the Docker volume.   
```
docker run -d --name webserver --mount source=<volume name>,target=targetdirectory -p 8000:8000  <image:name>
```
This will create a container and also mount the volume to the container directory. You can access the server from a browser.


# Minikube

## Install Minikube

Minikube requiremens:

- 2 CPUs or more
- 2GB of free memory
- 20GB of free disk space
- Virtual machine manager VirtualBox
- Make sure Docker is already installed



### Install in Linux

In this exercice, we will first install Minikube and Kubectl (the client). Then we will deploy a Minikube cluster. Finally, we will deploy an application in the cluster.
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```

### Install in Windows
```
winget install minikube
```

### Install Kubectl
```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
kubectl version –client
```

### Kubectl for Windows
```
curl -LO https://dl.k8s.io/release/v1.20.0/bin/windows/amd64/kubectl.exe
```


### Start minikube
```
minikube start —-vm-driver=none
```
It may take minutes to start the cluster. First cluster deployment requires downloading the Docker images of the Kubernetes components.

### Interact with Minikube cluster
```
kubectl get po -A
```
This will list out all the pods running in the cluster.

### Start dashboard
```
minikube dashboard
```

### List out all the pods
```
kubectl get po –A
```

### Stop the cluster
```
minikube stop
```

### Describe Kubernetes cluster
```
minikube describe 
```

### Delete the cluster
```
minikube delete
```


## Deploying a web Server
We will deploying Nginx webserver using kubectl
```
kubectl create deployment nginx --image=nginx
```

## Verify Nginx pods are running 
```
kubectl get po -A
```

## Exposing Nginx pod to outside the cluster
```
kubectl create service nodeport nginx --tcp=80:80
```

## Get summarize of the Nginx service
```
kubectl get svc
```
You can find the port used to expose the nginx pod.

## Access the Nginx web server 
```
curl http://cluster-ip:service-port
``` 
cluster-ip is the IP address of the master node and service-port is port used to expose the nginx service.



